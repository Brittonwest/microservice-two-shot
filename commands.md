Different commands you may need
You'll need to pull changes from GitLab. Sometimes, you may want to redo your database. You'll want to make migrations when you change your models. Here are the different commands for you.

[Pulling-changes]
When you want to pull changes that you teammate has pushed to GitLab, follow these steps:

* Stop all services
* Do the Git pull to get your teammate's changes
*** Run docker-compose build ***
*** Run docker-compose up ***

[Redo-database]
You may need to destroy your database just to get it to a good state. You can follow these steps to do that.

* Stop all services
*** Run docker container prune -f ***
*** Run docker volume rm pgdata ***
*** Run docker volume create pgdata ***
*** Run docker-compose up ***

[Migrations]
When you change your models, you'll need to make and run migrations. Do not stop your services.

*** Run docker exec -it «api-container-name» *** bash to connect to the running service that contains your API service.
*** Run python manage.py makemigrations *** at the container command prompt. Make sure it generates the migration you expected.
*** Run python manage.py migrate *** to apply the migration.
Stop your poller service using Docker Desktop.
Start your poller service using Docker Desktop.


[merge-into-main]

The steps
When a team member's branch is at a stable state and ready to be pushed to the main branch, follow these steps:

NOTE: all changes should be checked in to your dev-branch

*** (my-branch) $ git checkout main ***   # switch to main branch
*** (main) $ git pull ***                 # get latest from remote
*** (main) $ git checkout my-branch ***   # switch to dev branch
*** (my-branch) $ git merge main  ***     # merge latest into dev branch
... handle any merging here
... test out your code
*** (my-branch) $ git checkout main  ***  # switch to main branch
*** (main) $ git pull ***                 # test for changes on remote
... if no changes proceed,
... if changes go back to line 3
*** (main) $ git merge my-branch ***      # merge dev branch into main
*** (main) $ git push ***                 # push changes to the remote
*** (main) $ git checkout my-branch ***   # change back to dev branch
                                   # now do more work