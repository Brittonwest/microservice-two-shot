* Wardrobe API: provides Location and Bin RESTful API endpoints

* Database: the PostgreSQL database that will hold the data of all of the microservices

* React: the React-based front-end application where both you and your teammate will write the components to interact with your services

* Shoes API: the RESTful API to interact with Shoe resources

* Shoes Poller: the poller to poll the Wardrobe API for Bin resources

* Hats API: the RESTful API to interact with Hat resources

* Hats Poller: the poller to poll the Wardrobe API for Location resources

* Value Object --> closet_name, section_number, shelf_number


[SHOE-STEPS]
* [ ] Shoes/api -> 
  * [ ] install django app
    * [ ] create URLS, Views, Models
* [ ] Shoes/poll ->
  * [ ] Script file shoes/poll/poller.py
    * [ ] pull BIN data from wardrobe API
* [ ] Shoe resource
  * [ ] Manufacturer
  * [ ] Model Name
  * [ ] Color
  * [ ] Picture URL
  * [ ] BIN wardrobe
* [ ] RESTful API
  * [ ] Get list of shoes
  * [ ] create a new shoe
  * [ ] delete a shoe
* [ ] REACT 
  * [ ] List of shoes
  * [ ] shoe details
* [ ] REACT
  * [ ] From to create shoe
* [ ] Shoe Deletion Method
* [ ] Route Links to components