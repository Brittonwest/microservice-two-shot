from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

class Shoe(models.Model):
    shoe_name = models.CharField(max_length=20)
    shoe_manufacturer = models.CharField(max_length=20)
    shoe_color = models.CharField(max_length=20)
    shoe_picture_url = models.URLField(null=True)
    shoe_bin = models.ForeignKey(BinVO, related_name="shoes", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.shoe_name} - {self.shoe_manufacturer}"
    


