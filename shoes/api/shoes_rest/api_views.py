from .models import BinVO, Shoe
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

class BinDetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "shoe_name", 
        "shoe_manufacturer",
        "shoe_color",
        "shoe_picture_url", 
        "shoe_bin",
    ]
    def get_extra_data(self, o):
        return {"shoe_bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "shoe_name",
        "shoe_manufacturer",
        "shoe_color",
        "shoe_picture_url",
        "shoe_bin"
    ]
    encoders = {
        "shoe_bin": BinDetailEncoder(),
    }

@require_http_methods(["GET","POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoeDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin_href = content["shoe_bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["shoe_bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe, 
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({'message': 'Does not exist'})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            count,_ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count>0})
        except Shoe.DoesNotExist:
            return JsonResponse({'message': 'Does not exist'})
    else:
        try:
            content = json.loads(request.body)
            Shoe.objects.filter(id=pk).update(**content)
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response


