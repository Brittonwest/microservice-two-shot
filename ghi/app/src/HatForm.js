import React from 'React';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: "",
            style: "",
            color: "",
            pictureUrl: "",
            locations: []
        }
    }
    this.handleFabricChange = this.handleFabricChang.bind(this);
    this.handleStyleChange = this.handleStyleChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlepictureUrlChange = this.handlepictureUrlChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSUbmit.bind(this);
}

async handleSubmit(event){
    event.preventDefault();
    data.pictureUrl = data.pictureUrl;
    delete data.pictureUrl;
    delete data.locations;
    let hatUrl = `http://localhost:8090/api/locations/${data.location}/hats/`;
    let fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    let response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        const newHat = await response.json();
        const cleared = {
            fabric: "",
            style: "",
            color: "",
            pictureUrl: "",
        }
        this.setState(cleared)
    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
    }
    hsndelStyleChange(event) {
        const value = event.target.value;
        this.setState({style: value})
    }
    handleColorChange(event){
        const value = event.target.value;
        this.setState({color: value})
    }
    handlepictureUrlChange(event){
        const value = event.target.value;
        this.setState({pictureUrl: value})
    }
    handleLocationChange(event){
        const value = event.target.value;
        this.setState({location: value})
    }
    async conponentDidMount() {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            this.setState({locations: data.locations});
        }
    }
    render() {
        return (
            <div>

            </div>
        )
    }
}
export default HatForm;